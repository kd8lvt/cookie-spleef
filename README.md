# PLANS #
1. Honor block protection, such as WorldGuard, GriefPrevention, and Vanilla spawn protection. (Probably a good idea)
2. Countdown and warning to all players in the world.
3. Lobbies
4. Multiple games in one world, with different player sets (of course). (Config option, by default off because it probably would be buggy)

# Current Features #
The spleef bit works. When players fall into the void they don't get put into spectator... *yet*.

Game can be started, but not stopped.

Only two commands (so far):
/cs start | stop
(Also can type out /cookiespleef start | stop)