package io.github.funniray.cookiespleef.Listeners;

import io.github.funniray.cookiespleef.Classes.Cookie;
import net.minecraft.server.v1_9_R2.PacketPlayOutEntityDestroy;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_9_R2.entity.CraftPlayer;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Created by funniray on 6/14/17.
 */
public class onRightClick implements Listener {
    @EventHandler
    public void onRightClick(PlayerInteractEvent event){
        if (event.getItem() == null)
            return;
        if (event.getItem().getType().equals(Material.STONE_AXE)) {
            //Get a cookie itemstack, the player's eye location, and set the itemstack's amount to 1.
            ItemStack cookie = new ItemStack(Material.COOKIE);
            Location loc = event.getPlayer().getEyeLocation();
            cookie.setAmount(1);
            
            //Spawn an arrow in the player's world, and location.
            Arrow cookieArrow = event.getPlayer().getWorld().spawn(event.getPlayer().getLocation(), Arrow.class);
            
            //Set the arrow's shooter to the player, then spawn a cookie item at the player's eye location.
            cookieArrow.setShooter(event.getPlayer());
            Item cookieItem = event.getPlayer().getWorld().dropItem(loc, cookie);
            
            //Set the cookie item's pickup delay to 1000 ticks(?)
            //the cookieArrow's velocity to the direction the player it looking,
            //as well as the cookie item's, and set the arrow's silent tag to true.
            cookieItem.setPickupDelay(1000);
            cookieArrow.setVelocity(event.getPlayer().getLocation().getDirection().multiply(1.5));
            cookieItem.setVelocity(event.getPlayer().getLocation().getDirection().multiply(1.5));
            cookieArrow.setSilent(true);
            
            //Instiate the cookieArrow as a Cookie class.
            new Cookie(event.getPlayer(), cookieItem, cookieArrow);
            for (Player p : Bukkit.getServer().getOnlinePlayers()) {
                PacketPlayOutEntityDestroy packet = new PacketPlayOutEntityDestroy(cookieArrow.getEntityId());
                ((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
            }
            
            //Teleport the cookie arrow to the player's eye location.
            cookieArrow.teleport(loc);
        }
    }
}
