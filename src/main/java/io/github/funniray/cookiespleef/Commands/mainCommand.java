package io.github.funniray.cookiespleef.Commands;

import io.github.funniray.cookiespleef.Misc.startGame;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by funniray on 6/18/17.
 */
public class mainCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args[0] == "start") {        
            if (sender instanceof Player) {
                Player player = (Player) sender;
                startGame.start(player.getWorld());
            }
        } else if (args[0] == "end") {
            if (sender instanceof Player) {
                sender.sendMessage("§4You can't currently end a game through commands! This feature is NYI! (Not Yet Implemenmted)");
            }
        }
        return false;
    }
}
