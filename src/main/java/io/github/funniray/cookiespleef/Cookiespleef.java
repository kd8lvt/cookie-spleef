package io.github.funniray.cookiespleef;

import io.github.funniray.cookiespleef.Commands.start;
import io.github.funniray.cookiespleef.Listeners.onRightClick;
import io.github.funniray.cookiespleef.Misc.everyTick;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

public final class Cookiespleef extends JavaPlugin {



    @Override
    public void onEnable() {
        // Plugin startup logic
        
        //Get plugin manager, and register a right-click event
        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new onRightClick(),this);
        
        //Register commands
        this.getCommand("cookiespleef").setExecutor(new mainCommand());
        this.getCommand("cs").setExecutor(new mainCommand());
        
        //Get event scheduler, and set up an every-tick event
        BukkitScheduler scheduler = getServer().getScheduler();
        scheduler.scheduleSyncRepeatingTask(this,new everyTick(),0L,1L);
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }



}
