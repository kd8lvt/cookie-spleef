package io.github.funniray.cookiespleef.Misc;

import io.github.funniray.cookiespleef.Classes.Cookie;
import io.github.funniray.cookiespleef.Classes.Cookies;
import net.minecraft.server.v1_9_R2.EntityArrow;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_9_R2.entity.CraftArrow;
import org.bukkit.entity.Arrow;
import org.bukkit.util.BlockIterator;

import java.util.HashMap;

/**
 * Created by funniray on 6/18/17.
 */
public class everyTick implements Runnable {
    @Override
    public void run(){
        //Inistiate a list of cookieArrows, as well as a list to remove. 
        HashMap<Arrow,Cookie> cookies = Cookies.getCookies(); //List of cookies
        HashMap<Arrow,Cookie> toRemove = new HashMap<>(); //List of cookies to remove
        
        //Set up the cookieArrows, and teleport cookies to arrows, if needed. 
        for (Arrow arrow : cookies.keySet()){
            EntityArrow entityArrow = ((CraftArrow)arrow).getHandle(); //Gets Minecraft arrow entity, not Bukkit's
            Cookie cookie = cookies.get(arrow); //Get Cookie class connected to arrow
            if (!entityArrow.isInGround()) { //Checks if arrow is in a block
                cookie.getCookie().teleport(arrow.getLocation()); //If it isn't, then tp the cookie to arrow
                cookie.getCookie().setVelocity(arrow.getVelocity());
                continue;
            }
            World world = arrow.getWorld(); //otherwise, get arrow's wold
            
            //Raytrace from the arrow, and iterate through all the blocks.
            BlockIterator bi = new BlockIterator(world, arrow.getLocation().toVector(), arrow.getVelocity().normalize(), 0, 4);
            Block hit = null;

            while(bi.hasNext()) //While there are blocks to iterate through
            {
                hit = bi.next();
                if(hit.getType()!= Material.AIR) //Grass/etc should be added probably since arrows don't collide with them.
                    break;                       //That makes no sense Ethan. -^
            }
            
            //Remove the block the arrow hits
            //cookies.get(arrow).getPlayer().sendMessage(hit.getType().name());
            hit.setType(Material.AIR); //Remove block
            arrow.remove(); //Delete Arrow
            cookie.getCookie().remove(); //Delete Cookie
            toRemove.put(arrow,cookie); //Set to remove Cookie from list of Cookies
        }
        
        //Remove the arrow from the Cookie class' list.
        for (HashMap.Entry<Arrow, Cookie> entry : toRemove.entrySet()) {
            Cookies.remove(entry.getKey()); //Remove Cookie from list of Cookies
        }
    }
}
