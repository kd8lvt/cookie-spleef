package io.github.funniray.cookiespleef.Classes;

import org.bukkit.Location;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;

/**
 * Created by funniray on 6/14/17.
 */
public class Cookie {
    private Player player;
    private Item cookie;
    private Location initialLocation;
    private long timeThrown;
    private Arrow arrow;

    public Cookie(Player player, Item cookie, Arrow cookieArrow){
        this.player = player;
        this.cookie = cookie;
        this.initialLocation = player.getLocation();
        this.timeThrown = System.currentTimeMillis();
        this.arrow = cookieArrow;
        Cookies.add(cookieArrow,this);
    }

    public Item getCookie(){
        return this.cookie;
    }

    public Player getPlayer(){
        return this.player;
    }

    public long getTimeThrown(){
        return this.timeThrown;
    }

    public Location getInitialLocation(){
        return this.initialLocation;
    }

    public Arrow getArrow() {
        return this.arrow;
    }
}
