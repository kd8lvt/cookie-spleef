package io.github.funniray.cookiespleef.Classes;

import org.bukkit.entity.Arrow;

import java.util.HashMap;

/**
 * Created by funniray on 6/15/17.
 */
public class Cookies {
    private static HashMap<Arrow,Cookie> Cookies = new HashMap<>();

    public static void add(Arrow arrow, Cookie cookie){
        Cookies.put(arrow,cookie);
    }

    public static HashMap<Arrow,Cookie> getCookies(){
        return Cookies;
    }

    public static void setCookies(HashMap<Arrow,Cookie> cookies){
        Cookies = cookies;
    }

    public static void remove(Arrow arrow){
        Cookies.remove(arrow);
    }
}
